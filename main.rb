require_relative 'config/boot'
require 'subscription_manager'
require 'news_interpreter'
require 'sender'
require 'lib/filters'

manager = SubscriptionManager.new
news_interpreter = NewsInterpreter.new
sender = Sender.new

user = %x{whoami}.chomp
puts "Hello, #{user}. Welcome to Subscoito."

puts 'Tip: use "cd" with manager, news_interpreter, or sender as a parameter.'

pry
