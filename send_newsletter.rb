require_relative 'config/boot'
require 'subscription_manager'
require 'news_interpreter'
require 'sender'
require 'lib/filters'

sender = Sender.new
time = Time.now.strftime('%d/%m/%Y - %H:%M')

puts "#{time} - Enviando newletter"
sender.send_newsletter
