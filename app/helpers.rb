require 'sanitize'

module Helpers
  def strip_html(text)
    Sanitize.clean(text)
  end

  def formatted_date
    Time.now.strftime('%Y%m%d')
  end

  def prepare_permalink(link)
    site_root = 'http://tribunadonorte.com.br'
    permalink = link.prepend(site_root)
    permalink << '?utm_campaign=noticias&utm_source=' + formatted_date
  end
end
