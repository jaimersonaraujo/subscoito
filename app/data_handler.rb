class DataHandler

  def initialize
    environment = ENV['RACK_ENV'] || 'development'
    @dbconfig = YAML.load(File.read('./config/database.yml'))[environment]
  end

  def get_subscribers
    fields = 'cli_id as id, cli_nome as nome, cli_email as email'
    mailings = 'cli_newsletter as newsletter'
    query = "SELECT #{fields}, #{mailings} FROM cliente WHERE cli_newsletter = 1"

    make_query(query)
  end

  def get_news
    rank = get_ranking.map { |n| n['not_id'] }
    not_ids = rank.join(', ')
    fields = 'not_id, edi_id_fk, not_titulo, not_texto, not_permalink'
    query = "SELECT #{fields} FROM noticia WHERE not_id IN(#{not_ids})"
    puts query

    make_query(query)
  end

  def get_ranking
    query = "SELECT not_id FROM noticiaRanking ORDER BY htotal DESC LIMIT 10"
    make_query(query, 'noticias_stats')
  end

  protected
  def make_query(query, database=nil)
    begin
      db = Mysql2::Client.new(
        host: @dbconfig['host'],
        username: @dbconfig['username'],
        password: @dbconfig['password'],
        database: database || @dbconfig['database'],
      )
      result = db.query(query)
    rescue Mysql2::Error => ex
      binding.pry
      puts 'Could not connect to database. Sorry :('
    ensure
      db.close
    end

    result
  end
end
