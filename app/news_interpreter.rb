require_relative 'helpers'

class NewsInterpreter
  include Helpers

  def initialize
    @db = DataHandler.new
  end

  def news
    @db.get_news.map do |noticia|
      noticia['not_permalink'] = prepare_permalink(noticia['not_permalink'])
      noticia['not_texto'] = summary(strip_html(noticia['not_texto']))
      noticia
    end
  end

  def newsletter_json(recipient=nil)
    hash = {
      "title" => "Notícias mais lidas da manhã",
      "type" => "newsletter",
      "send_at" => Time.now.to_s,
      "news" => news_json
    }
    hash['recipients'] = recipient if recipient
    hash
  end

  private

  def summary(text)
    text.truncate_words(60) << '...'
  end

  def news_json
    hash = {}
    news.each_with_index do |n, index|
      hash[index.to_s] = n
    end
    hash
  end

end
