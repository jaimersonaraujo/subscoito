require 'httparty'

class Sender

  def initialize
    @news = NewsInterpreter.new
    @manager = SubscriptionManager.new
    @responses = []
    @failed_emails = []
  end

  def send_newsletter(recipient=nil)
    puts 'Connecting to MAYA...'
    uri = 'http://162.243.109.105/v1/send_mail'
    response = HTTParty.post(uri, :body => {"campaign" => @news.newsletter_json(recipient)})
    puts response.body
  end

  def send_recipients
    puts 'Connecting to MAYA...'
    uri = 'http://162.243.109.105/v1/recipients'
    @manager.clients_list.each_with_index do |client, index|
      client[:name] = client[:name].force_encoding('UTF-8')
      response = HTTParty.post(uri, :body => {userinfo: client}).body
      check_success(response, client)
    end
    puts

    [@responses, @failed_emails]
  end

  private
  def check_success(response, client)
    unless /success/.match(response)
      @responses << response
      @failed_emails << client
      print 'F'
    else
      print '.'
    end
  end
end
