require 'data_handler'

class SubscriptionManager
  def initialize
    @db = DataHandler.new
  end

  def clients_list
    json_list = []
    @db.get_subscribers.each do |client|
      json_list << mount_hash(client)
    end
    json_list
  end

  protected
  def mount_hash(client)
    {
      client_id: client['id'],
      email: client['email'],
      name: client['nome'],
      mailings: ['newsletter']
    }
  end
end
