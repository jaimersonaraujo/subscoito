require 'spec_helper'
require 'lib/filters'

describe String do
  it 'does not alter original string without !' do
    text = 'The quick fox jumps over the lazy dog.'
    text.truncate_words(3)
    expect(text).to eql('The quick fox jumps over the lazy dog.')
  end

  it 'does alter original string with !' do
    text = 'The quick fox jumps over the lazy dog.'
    text.truncate_words!(3)
    expect(text).to eql('The quick fox')
  end
end
