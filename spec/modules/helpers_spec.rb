require 'spec_helper'
require 'helpers'

# define dummy class to include methods
class Helper
  include Helpers
end

describe Helpers do
  let(:helper) { Helper.new }

  describe '#formatted_date' do
    it 'returns current date in the format yyyymmdd' do
      expect(helper.formatted_date).to eql(Time.now.strftime('%Y%m%d'))
    end
  end

  describe '#strip_html' do
    it 'removes html formatting tags' do
      text = '<strong>Hello</strong>, my name is <i>Johnny</i>'
      expect(helper.strip_html(text)).to eql('Hello, my name is Johnny')
    end

    it 'removes inline styling' do
      text = '<div style="background: red;">The quick fox jumps over the lazy dog</div>'
      expect(helper.strip_html(text).strip).to eql('The quick fox jumps over the lazy dog')
    end
  end

  describe '#prepare_permalink' do
    it 'prepends site root' do
      link = '/noticias'
      expect(helper.prepare_permalink(link)).to match(/^http:\/\//)
    end

    it 'appends campaign tracker' do
      link = '/blogs'
      expect(helper.prepare_permalink(link)).to match(/utm_campaign=noticias/)
    end
  end
end
