set :output, '/var/log/subscoito.log'

file_path = File.join(Dir.pwd, 'send_newsletter.rb')

every :day, at: '11:30' do
  command "ruby #{file_path}"
end
