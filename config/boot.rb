autoload :YAML, 'yaml'

require 'pry'
require 'mysql2'
require 'json'

$LOAD_PATH.unshift File.expand_path('../../app/', __FILE__)
$LOAD_PATH.unshift File.expand_path('../../', __FILE__)
