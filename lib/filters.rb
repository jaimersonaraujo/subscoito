class String
  def truncate_words(length)
    dup.truncate_words!(length)
  end

  def truncate_words!(length)
    words = self.split
    words = words[0, length]
    truncated = words.join(' ')
    self.sub!(/(.*)/, truncated)
  end
end
